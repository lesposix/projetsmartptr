#ifndef __CNODE_H__
#define __CNODE_H__

/*!
 * \file CNode.h
 * \brief List's node
 * \author {Adrien, Arnaud, Thomas, Valerian, Vincent}
 * \version 1.0
 */

#include <iostream>
#include <memory>

/*!
 *  \brief Template
 *
 *  Class template
 */
template <typename T>

/*! \class CNode
 * \brief Class representing the list's node
 *
 *  This class instantiate the node
 */
class CNode
{
	 public:
		/*!
		 *  \brief Constructor
		 *
		 *  CNode constructor
		 *
		 *  \param[in] data : data contained in the node
		 *  \param next : pointer to next element
		 *  \param previous : pointer to previous element
		 */
		CNode(const T& data = T(), const std::shared_ptr<CNode<T>> next = nullptr, const std::shared_ptr<CNode<T>> previous = nullptr);

        /*!
         * \brief Destructor
         *
         *  CNode destructor
         *
         */
		~CNode();

        /*!
         *  \brief Contents accessor
         *
         *  Accessor to node's contents
         *
         * 	\return retrieve the contents of the node
         */
        T getData() const;

        /*!
         * \brief Next accessor
         *
         * Accessor to the node after the current node
         *
         * \return Return pointer pointing to next node
         */
        std::shared_ptr<CNode<T>> getNext() const;

        /*!
         * \brief Previous accessor
         *
         * Accessor to the node before the current node
         *
         * \return Return pointer pointing to previous node
         */
        std::shared_ptr<CNode<T>> getPrevious() const;

        /*!
         * \brief Contents mutator
         *
         * Mutator instantiating node's contents
         *
         *  \param Data instantiating data member m_Data in class CNode
         */
        void setData(T);

        /*!
         * \brief Next mutator
         *
         * Mutator instantiating pointer pointing to next node
         *
         * \param Pointer instantiating data member m_Suivant in class CNode
         */
        void setNext( const std::shared_ptr<CNode<T>>& next);

        /*!
         * \brief Previous mutator
         *
         * Mutator instantiating pointer pointing to previous node
         *
         * \param Pointer instantiating data member m_Precedent in class CNode
         */
        void setPrevious(const std::shared_ptr<CNode<T>>& previous);

	 private:
		T                       m_Data; /*!< Node's data*/
		std::shared_ptr<CNode<T>> m_Next; /*!< Pointer to next node*/
		std::shared_ptr<CNode<T>> m_Previous; /*!< Pointer to previous node*/

};//Class CNode

#include "CNode.hxx"
#endif /*__CNODE_H__*/
