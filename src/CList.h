#ifndef CLIST_H
#define CLIST_H

#include"CNode.h"
#include <memory>
#include <iostream>

namespace nsSdD
{
    template<typename T>
    class CList
    {

    public:
        /** Default constructor */
        explicit CList();

        /**
		*  @name	CList(size_type n)
		*  @brief  Creates a CList with default constructed elements.
		*  @param  n  The number of elements to initially create.
		*
		*  This constructor fills the %CList with @a n default constructed elements.
		*/
        explicit CList(size_t n);

        /**
		*  @name   CList(size_type n, const value_type& val)
		*  @brief  Creates a %CList with copies of an exemplar element.
		*  @param  n  The number of elements to initially create.
		*  @param  val  An element to copy.
		*
		*  This constructor fills the %CList with @a n copies of @a val.
		*/
        explicit CList(size_t n,T value);

        /** Default destructor */
        ~CList();

        /** Access m_Head
         * \return The current value of m_Head
         */
        std::shared_ptr<CNode<T>> GetHead();

        /** Set m_Head
         * \param val New value to set
         */
        void SetHead(std::shared_ptr<CNode<T>> val);

        /** Access m_Tail
         * \return The current value of m_Tail
         */
        std::shared_ptr<CNode<T>> GetTail();

        /** Set m_Tail
         * \param val New value to set
         */
        void SetTail(std::shared_ptr<CNode<T>> val);

        /** Access m_Count
         * \return The current value of m_Count
         */
        std::size_t size();

        /** Set m_Count
         * \param val New value to set
         */
        void SetCount(std::size_t val);

        /** get Data stored in m_Head
         * \return Data in m_Head
         */
        T front();

        /** get Data stored in m_Tail
         * \return Data in m_Tail
         */
        T back();

        /** Check if the CList is empty
         * \return true if CList is empty
         */
        bool empty();

        /** Add a value at the front of the CList
         * \param value new value to add to the CList
         */
        void push_front(T value);

        /** Add a value at the back of the CList
         * \param value new value to add to the CList
         */
        void push_back(T value);

        /** Print from m_Head to m_Tail the CList
        */
        void printForward();

        /** Print from m_Head to m_Tail the CList
        */
        void printReverse();

        /** Delete each node in the Clist until CList is empty
        */
        void clear();

        /** Delete last node in the CList
        */
        void pop_back();

        /** Delete first node in the CList
        */
        void pop_front();

        /** get the next node after the given node in the CList
         * \param @node a pointer in the CList
         * \return A pointer to the next of @node
         */
        std::shared_ptr<CNode<T>> getSuivant(std::shared_ptr<CNode<T>>);

    private:
        std::shared_ptr<CNode<T>> m_Head; //!< Member variable "m_Head"
        std::shared_ptr<CNode<T>> m_Tail; //!< Member variable "m_Tail"
        std::size_t m_Count; //!< Member variable "m_Count"
    };
}
#include "CList.hxx"
#endif // CLIST_H
