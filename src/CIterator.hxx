#ifndef __CITERATOR_HXX__
#define __CITERATOR_HXX__

#include "CIterator.h"

/*!
 *  \brief definition of a macro TEMP
 *
 *  Which allow us to simplify the code, corresponding to the template <typename T>
 */
#define TEMP template <typename T>

/*!
 *  \brief definition of a macro INL
 *
 *  Which allow us to simplify the code,, corresponding to "inline", in accordance with the IUT coding conventions
 */
#define INL inline
TEMP
INL CIterator<T>::Iterator(std::shared_ptr<CNoeud<T> > Noeud) : m_Noeud(Noeud) {};//Iterator

void CIterator<T>::operator++(){ m_Noeud = m_Noeud->m_Next; } //operator++

void CIterator<T>::operator++(int){ m_Noeud = m_Noeud->m_Next; } //operator++

bool CIterator<T>::operator!=(Iterator<CNoeud> compVal){ return !(m_Noeud == compVal.m_Noeud); }//operator !=

bool CIterator<T>::operator==(Iterator<CNoeud> compVal){ return (m_Noeud == compVal.m_Noeud); } //operator==

T CIterator<T>::operator*(){	return m_Noeud->m_Data; } //operator*

CIterator<CNoeud> CIterator<T>::operator+(int i) {

    CIterator<CNoeud> iter = std::shared_ptr<this>;
    for (int i = 0; i < _i; ++i)
    {
        if (iter.m_Noeud) //If there's something to move onto...
            ++iter;
        else
            break;
    }
    return iter; //Return regardless of whether its valid...
}//operator +

#undef INL
#undef TEMP
#endif /* __CITERATOR_HXX__ */