#ifndef __CNODE_HXX__
#define __CNODE_HXX__

/*!
 * \file CNode.hxx
 * \brief Noeud de notre list
 * \author {Adrien, Arnaud, Thomas, Valerian, Vincent}
 * \version 1.0
 */

#include <iostream>
#include <memory>
#include "CNode.h"

/*!
 *  \brief definition of a macro TEMP
 *
 *  Which allow us to simplify the code, corresponding to the template <typename T>
 */
#define TEMP template <typename T>

/*!
 *  \brief definition of a macro INL
 *
 *  Which allow us to simplify the code,, corresponding to "inline", in accordance with the IUT coding conventions
 */
#define INL inline

/*!
 *  \brief Generic inline constructor
 *
 *  CNode constructor
 *
 *  \param data : data contained in the node
 *  \param next : pointer to next element
 *  \param previous : pointer to previous
 */
TEMP
INL CNode<T>::CNode(const T& data, const std::shared_ptr<CNode<T>> next /*= Null*/,  const std::shared_ptr<CNode<T>> previous /*=NULL*/)
                : m_Data(data), m_Next(next), m_Previous(previous) {};
//CNode()

/*!
 * \brief Destructor
 *
 *  Destructor of the node
 *
 */
TEMP
INL CNode<T>::~CNode() {}//~CNode()

/*!
 *  \brief Contents accessor
 *
 *  Accessor to node's contents
 *
 * 	\return Retrieve the contents of the node
 */
TEMP
INL T CNode<T>::getData() const {

    return m_Data;
}//getData()

/*!
 * \brief Next accessor
 *
 * Accessor to the node after the current node
 *
 * \return Return pointer pointing to next node
 */
TEMP
INL std::shared_ptr<CNode<T>> CNode<T>::getNext() const {

    return m_Next;

}//getNext()

/*!
 * \brief Previous accessor
 *
 * Accessor to the node before the current node
 *
 * \return Return pointer pointing to previous node
 */
TEMP
INL std::shared_ptr<CNode<T>> CNode<T>::getPrevious() const {

    return m_Previous;

}//getPrevious

/*!
 * \brief Contents mutator
 *
 * Mutator instantiating node's contents
 *
 *  \param Data instantiating data member m_Data in class CNode
 */
TEMP
INL void CNode<T>::setData(T Info) {

    m_Data = Info;
}//setData()

/*!
 * \brief Next mutator
 *
 * Mutator instantiating pointer pointing to next node
 *
 * \param Pointer instantiating data member m_Suivant in class CNode
 */
TEMP
INL void CNode<T>::setNext(const std::shared_ptr<CNode<T>>& next) {

    m_Next = next;
}//setNext

/*!
 * \brief Previous mutator
 *
 * Mutator instantiating pointer pointing to previous node
 *
 * \param Pointer instantiating data member m_Precedent in class CNode
 */
TEMP
INL void CNode<T>::setPrevious(const std::shared_ptr<CNode<T>>& previous) {

    m_Previous = previous;
}//setPrevious

#undef TEMP
#undef INL
#endif /*__CNODE_HXX*/
