#ifndef __CITERATOR_H__
#define __CITERATOR_H__

template <typename T>

class CIterator
{
private :
	std::shared_ptr<CNoeud<T> > m_Noeud;

	CIterator(std::shared_ptr<CNoeud<T> > Noeud);
	
public:
	void operator++();
	void operator++(int);
	bool operator!=(CIterator<CNoeud<T> > compVal);
	bool operator==(CIterator<CNoeud<T> > compVal);
	T operator*();
	CIterator<CNoeud> operator+(int _i);
};

#include "CIterator.hxx"

#endif // __CITERATOR_H__