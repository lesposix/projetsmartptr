#include <iostream>
#include <memory>
#include "CNode.h"
#include "CList.h"

using namespace std;
using namespace nsSdD;

int main() {
    cout<<"**********Debut du test**********"<<endl<<endl;

    cout<<"*****Debut du test de CNode*****"<<endl;
	shared_ptr<CNode<int>> a(new CNode<int>(7));
	shared_ptr<CNode<float>> b(new CNode<float>(54));
	shared_ptr<CNode<double>> c(new CNode<double>(988));
	shared_ptr<CNode<string>> d(new CNode<string>("bonjour"));
	cout<<"Valeur du node a (doit valoir 7): "<< a -> getData() <<endl;
	cout<<"Valeur du node b (doit valoir 54): "<< b -> getData() <<endl;
	cout<<"Valeur du node c (doit valoir 988): "<< c -> getData() <<endl;
	cout<<"Valeur du node d (doit valoir 'bonjour'): "<< d -> getData() <<endl;
	cout<<"*****Fin du test de CNode*****"<<endl<<endl;

    cout<<"*****Debut du test de CList*****"<<endl;
	CList<int> list1;
	CList<string> list3(4,"raoul");
	CList<string> list4(66);
    list1.push_back(7);
    list1.push_back(9);
    list1.push_front(80);
    list1.push_back(50);
	cout<<"Valeur de Head de list1 (doit valoir 80): "<<list1.front()<<endl;
	cout<<"Valeur de Tail de list1 (doit valoir 50): "<<list1.front()<<endl;
	cout<<"size de  list3(doit valoir 4): "<< list3.size()<<endl;
	cout<<"size de  list4 (doit valoir 66): "<< list4.size()<<endl;
	list4.clear();
	cout<<"size de  list4 (doit valoir 0): "<<list4.size()<<endl;
	list1.pop_back();
	list1.pop_front();
	cout<<"size de  list1 (doit valoir 2): "<< list1.size()<<endl;
	cout<<"Valeur de Tail de list1 (doit valoir 9): "<<list1.back()<<endl;
	cout<<"Valeur de Head de list1 (doit valoir 7): "<<list1.front()<<endl;
	cout<<"*****Fin du test de CList*****"<<endl<<endl;

	cout<<"**********Fin du test**********"<<endl;
	return 0;
}
