#ifndef __CLIST_HXX__
#define __CLIST_HXX__

/*!
 * \file CNode.hxx
 * \brief Noeud de notre list
 * \author {Adrien, Arnaud, Thomas, Valerian, Vincent}
 * \version 1.0
 */

#include <iostream>
#include <memory>
#include "CList.h"


/*!
 *  \brief definnition d'une macro TEMP
 *
 *  Macro permettant d'alléger le code, correspondant a template <typename T>
 */
#define TEMP template <typename T>

/*!
 *  \brief definnition d'une macro CLIST
 *
 *  Macro permettant d'alléger le code, correspondant a nsSdD::CList<T>
 */
#define CLIST nsSdD::CList<T>

/*!
 *  \brief definition d'une macro INL
 *
 *  Macro permettant d'alléger le code, correspond a inline, norme de l'iut des lors que l'on utilise la généricité
 */
#define INL inline

using std::shared_ptr;

TEMP
CLIST::CList()
    : m_Head(nullptr), m_Tail(nullptr), m_Count(0) {}

TEMP
CLIST::CList(size_t n)
{
    shared_ptr<CNode<T>> newNode(new CNode<T>(T()));
    m_Count=0;
    m_Head = newNode;
    m_Tail= newNode;
    ++m_Count;
    for(unsigned i=1; i<n; ++i)
    {
        //shared_ptr<CNode<T>> newNode(new CNode<T>(value,nullptr,m_Tail));
        shared_ptr<CNode<T>> newNode(new CNode<T>(T()));
        m_Tail->setNext(newNode);
        newNode->setPrevious(m_Tail);
        m_Tail = newNode;
        ++m_Count;
    }
}

TEMP
CLIST::CList(size_t n,T value)
{
    shared_ptr<CNode<T>> newNode(new CNode<T>(value));
    m_Count=0;
    m_Head = newNode;
    m_Tail= newNode;
    ++m_Count;
    for(unsigned i=1; i<n; ++i)
    {
        //shared_ptr<CNode<T>> newNode(new CNode<T>(value,nullptr,m_Tail));
        shared_ptr<CNode<T>> newNode(new CNode<T>(value));
        m_Tail->setNext(newNode);
        newNode->setPrevious(m_Tail);
        m_Tail = newNode;
        ++m_Count;
    }
}

TEMP
CLIST::~CList() {}


/** Access m_Head
 * \return The current value of m_Head
 */
TEMP
INL shared_ptr<CNode<T>> CLIST::GetHead()
{
    return m_Head;
}

/** Set m_Head
 * \param val New value to set
 */
TEMP
INL void CLIST::SetHead(shared_ptr<CNode<T>> val)
{
    m_Head = val;
}

/** Access m_Tail
 * \return The current value of m_Tail
 */
TEMP
INL shared_ptr<CNode<T>> CLIST::GetTail()
{
    return m_Tail;
}


/** Set m_Tail
* \param val New value to set
*/
TEMP
INL void CLIST::SetTail(shared_ptr<CNode<T>> val)
{
    m_Tail = val;
}


/** Access m_Count
 * \return The current value of m_Count
 */
TEMP
INL std::size_t CLIST::size()
{
    return m_Count;
}

/** Set m_Count
 * \param val New value to set
 */
TEMP
INL void CLIST::SetCount(std::size_t val)
{
    m_Count = val;
}

/** get Data stored in m_Head
 * \return Data in m_Head
 */
TEMP
INL T CLIST::front()
{
    if(m_Count==0)
    {
        std::cout<<"CList is empty !";
        return 0;
    }
    else return m_Head->getData();
}

/** get Data stored in m_Tail
 * \return Data in m_Tail
 */
TEMP
INL T CLIST::back()
{
    if(m_Count==0)
    {
        std::cout<<"CList is empty !";
        return 0;
    }
    else return m_Tail->getData();
}

/** Check if the CList is empty
 * \return true if CList is empty
 */
TEMP
INL bool CLIST::empty()
{
    return (m_Count == 0);
}

/** Add a value at the front of the CList
 * \param value new value to add to the CList
 */
TEMP
void CLIST::push_front(T value)
{
    shared_ptr<CNode<T>> newNode(new CNode<T>(value));
    if( m_Count== 0)
    {
        m_Head = newNode;
        m_Tail = newNode;
        ++m_Count;
    }
    else
    {
        m_Head->setPrevious(newNode);
        newNode->setNext(m_Head);
        m_Head = newNode;
        ++m_Count;
    }
}

/** Add a value at the back of the CList
 * \param value new value to add to the CList
 */
TEMP
void CLIST::push_back(T value)
{
    shared_ptr<CNode<T>> newNode(new CNode<T>(value));
    if( m_Count == 0)
    {
        m_Head = newNode;
        m_Tail= newNode;
        ++m_Count;
    }
    else
    {
        m_Tail->setNext(newNode);
        newNode->setPrevious(m_Tail);
        m_Tail = newNode;
        ++m_Count;
    }
}//push_back()

/** Print from m_Head to m_Tail the CList
 */
TEMP
void CLIST::printForward()
{
    if(m_Count==0)std::cout<<"CList is empty !";
    else
    {
        shared_ptr<CNode<T>> tmp=m_Head;
        while(tmp != nullptr)
        {
            std::cout<<tmp->getData()<<" ";
            tmp=tmp->getNext();
        }
    }
    std::cout<<std::endl;
}

/** Print from m_Head to m_Tail the CList
 */
TEMP
void CLIST::printReverse()
{
    if(m_Count==0)std::cout<<"CList is empty !";
    else
    {
        shared_ptr<CNode<T>> tmp=m_Tail;
        while(tmp != nullptr)
        {
            std::cout<<tmp->getData()<<" ";
            tmp=tmp->getPrevious();
        }
    }

    std::cout<<std::endl;
}

/** Delete each node in the Clist until CList is empty
 */
TEMP
void CLIST::clear()
{
    while(m_Count!=0)
    {
        if(m_Count==1)
        {
            m_Tail=nullptr;
            m_Head=nullptr;
            --m_Count;
        }
        else
        {
            shared_ptr<CNode<T>> delNode = m_Tail;
            m_Tail = delNode->getPrevious();
            m_Tail->setNext(nullptr);
            delNode.reset();
            --m_Count;
        }
    }

}

/** Delete last node in the CList
 */
TEMP
void CLIST::pop_back()
{
    if(m_Count==1)
    {
        m_Tail=nullptr;
        m_Head=nullptr;
        --m_Count;
    }
    else
    {
        shared_ptr<CNode<T>> delNode = m_Tail;
        m_Tail = delNode->getPrevious();
        m_Tail->setNext(nullptr);
        delNode.reset();
        --m_Count;
    }
}

/** Delete first node in the CList
 */
TEMP
void CLIST::pop_front()
{

    if(m_Count==1)
    {
        m_Tail=nullptr;
        m_Head=nullptr;
        --m_Count;
    }
    else
    {
        shared_ptr<CNode<T>> delNode = m_Head;
        m_Head = delNode->getNext();
        m_Head->setPrevious(nullptr);
        delNode.reset();
        --m_Count;
    }
}

/** get the next node after the given node in the CList
 * \param @node a pointer in the CList
 * \return A pointer to the next of @node
 */
TEMP
INL std::shared_ptr<CNode<T>> CLIST::getSuivant(std::shared_ptr<CNode<T>> node)
{
    return node->getNext();
}

#undef TEMP
#undef INL
#endif /*__CLIST_HXX*/
